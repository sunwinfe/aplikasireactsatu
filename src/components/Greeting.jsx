import React from 'react';

//Penggunaan Komponen Function
function Greeting(props) {
 return (
    <h2>Selamat datang di aplikasi {props.title}</h2>
 )
}

export default Greeting;