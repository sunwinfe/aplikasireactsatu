import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo"></img>
				<h1> Hello World!</h1>
				<h3>Aplikasi ini dibuat dengan create-react-app</h3>
			</header>
		</div>
	);
}

export default App;
