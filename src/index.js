import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.css';
import logo from './logo.svg';
// import App from './App';
import Greeting from './components/Greeting';
import Input from './components/Input';
import * as serviceWorker from './serviceWorker';

const nama = 'React JS';
const elemen_jsx = (
	<h2 className="introduction">Halo ini merupakan latihan {nama}</h2>
);

const create_element = React.createElement(
	'h2',
	{ className: 'introduction' },
	'Selamat Datang di Pengenalan React Js'
);

const judul = 'Mengenal Pemrograman React JS';
const tahun = 2017;
const author = 'Jubilee Enterprise';
const deskripsi = 'Mengupas kode dan fungsi react js untuk mempercantik Web';
const ekspresi_js = (
	<div>
		<h1>Informasi Buku</h1>
		<h2 className="warna-judul">{judul}</h2>
		<h3>Tahun Pembuatan : {tahun}</h3>
		<h3>Author : {author}</h3>
		<h3>Deskripsi : {deskripsi} </h3>
	</div>
);

const elemen_atribut = <img src={logo} className="img" alt="logo" />;

ReactDOM.render(
	<React.StrictMode>
		<header className="App-header">
			<img src={logo} className="App-logo" alt="logo" />
			<Greeting title="React" />
			<form>
				<Input placeholder="Username" />
				<Input placeholder="Password" />
				<button className="button" type="submit">
					Login
				</button>
			</form>
		</header>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
